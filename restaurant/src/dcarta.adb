with Ada.Strings.Unbounded.Text_IO;
use Ada.Strings.Unbounded.Text_IO;

package body dcarta is
   procedure carta_buida (c: out carta) is
      e: existencia renames c.e;
      co: contenido renames c.co;
   begin
      -- Recorremos el array de categorias e inicializamos los tries y la
      -- estructura de arrays indexados por claves
      for k in tcategoria loop
         e(k):=false;
         cvacio(co(k));
      end loop;
   end carta_buida;

   procedure posar_element (c: in out carta; cat: in tcategoria; k: in tcodi; nom: in Unbounded_String) is
      co: contenido renames c.co;
      e: existencia renames c.e;
   begin
      -- Introducimos dentro del trie el nombre del plato, dada una categoria
      poner(co(cat), k, nom);
      -- Indicamos que hay un elemento dentro de la estructura
      e(cat):= true;
      -- Capturamos las excepciones del trie
   exception
      when dtrie_item.already_exists => raise dcarta.already_exists;
      when dtrie_item.space_overflow => raise dcarta.space_overflow;
   end posar_element;

   procedure imprimir_elements(c: in carta; cat: in tcategoria ) is
      co: contenido renames c.co;
      e: existencia renames c.e;
      it: iterator;
      k: tcodi;
      x: Unbounded_String;
   begin
     -- Si esta vacio el trie de la categoria seleccionada,
     -- elevamos error no existe
      if not e(cat) then raise does_not_exist; end if;
      -- Recorremos el trie con el iterador y vamos imprimiendo por
      -- pantalla los platos y bebidas de una categoria dada
      first (co(cat), it);
      while is_valid(it) loop
         get(co(cat), it, k, x);
         Put_Line(x);
         next(co(cat), it);
      end loop;
      -- Capturamos la excepcion del trie
   exception
      when dtrie_item.bad_use => raise dcarta.bad_use;
   end imprimir_elements;

   procedure eliminar_elements(c: in out carta; cat: in tcategoria; k: in tcodi) is
      co: contenido renames c.co;
      e: existencia renames c.e;
   begin
     -- Si esta vacio el trie de la categoria seleccionada,
     -- elevamos error no existe
      if not e(cat) then raise does_not_exist; end if;
      -- Borramos el plato dentro del trie dada una categoria y una clave
      -- al borrar el plato, tambien se borran todos los comentarios asociados
      borrar(co(cat), k);
      -- Si el trie esta vacio, indicamos en el array indexado por claves que
      -- no hay elementos dada la categoria
      if is_empty(co(cat)) then e(cat):=false; end if;
      -- Capturamos la excepcion del trie
   exception
      when dtrie_item.does_not_exist => raise dcarta.does_not_exist;
   end eliminar_elements;

   procedure posar_comentari(c: in out carta; cat: in tcategoria; k: in tcodi; q : in tqualificacio; comentari: in Unbounded_String) is
      co: contenido renames c.co;
      e: existencia renames c.e;
   begin
      -- Si esta vacio el trie de la categoria seleccionada,
      -- elevamos error no existe
      if not e(cat) then raise does_not_exist; end if;
      -- Introducimos un comentario dentro del trie, dada una categoria,
      -- una clave y una calificacion
      put_comment(co(cat), k, q, comentari);
      -- Capturamos las excepciones del trie
   exception
      when dtrie_item.does_not_exist => raise dcarta.does_not_exist;
      when dtrie_item.space_overflow => raise dcarta.space_overflow;
   end posar_comentari;

   function consultar_comentari (c: in carta; cat : in tcategoria; k: in tcodi; q: in tqualificacio ) return Unbounded_String is
      co: contenido renames c.co;
      e: existencia renames c.e;
      comentari: Unbounded_String;
   begin
      -- Si esta vacio el trie de la categoria seleccionada,
      -- elevamos error no existe
      if not e(cat) then raise does_not_exist; end if;
      -- Buscamos el ultimo comentario introducido de una categoria, clave
      -- y cualificacion dadas
      query_comment(co(cat), k, q, comentari);
      return comentari;
      -- Capturamos las excepciones del trie
    exception
      when dtrie_item.bad_use => raise dcarta.bad_use;
      when dtrie_item.does_not_exist => raise dcarta.does_not_exist;
   end consultar_comentari;

   function existeix_comentari (c: in carta; cat: in tcategoria; k: in tcodi; q: in tqualificacio) return Boolean is
      co: contenido renames c.co;
      e: existencia renames c.e;
   begin
      -- Si esta vacio el trie de la categoria seleccionada,
      -- elevamos error no existe
      if not e(cat) then raise does_not_exist; end if;
      -- Miramos si existe o no comentarios de una categoria, clave
      -- y calificacion dadas
      return exist_comment(co(cat), k, q);
      -- Capturamos la excepcion del trie
   exception
      when dtrie_item.does_not_exist => raise dcarta.does_not_exist;
   end existeix_comentari;

   procedure eliminar_comentari (c: in out carta; cat: in tcategoria; k: in tcodi; q : in tqualificacio) is
      co: contenido renames c.co;
      e: existencia renames c.e;
   begin
      -- Si esta vacio el trie de la categoria seleccionada,
      -- elevamos error no existe
      if not e(cat) then raise does_not_exist; end if;
      -- Eliminamos el comentario mas antiguo de una categoria, clave
      -- y calificacion dadas
      delete_comment(co(cat), k, q);
      -- Capturamos las excepciones del trie
   exception
      when dtrie_item.bad_use => raise dcarta.bad_use;
      when dtrie_item.does_not_exist => raise dcarta.does_not_exist;
   end eliminar_comentari;
end dcarta;
