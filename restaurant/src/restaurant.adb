with dcarta, Ada.Text_IO;
use dcarta, Ada.Text_IO;
with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;
with Ada.Strings.Unbounded.Text_IO;
use Ada.Strings.Unbounded.Text_IO;

procedure Restaurant is
   -- Tipo carta
   carta_1: carta;
   -- Claves de los platos
   key1, key2, key3, key4, key5: tcodi;
   key6, key7, key8, key9, key10: tcodi;
   key11, key12, key13, key14, key15: tcodi;
   -- Nombre de los platos
   str1, str2, str3, str4, str5, x: Unbounded_String;
   str6, str7, str8, str9, str10: Unbounded_String;
   str11, str12, str13, str14, str15: Unbounded_String;
   -- Comentarios
   strc1, strc2, strc3, strc4, strc5: Unbounded_String;

   procedure print_comments(c: in out carta; cat: in tcategoria; k: in tcodi; str: Unbounded_String) is
      -- Booleano para imprimir por pantalla si no hay comentarios
      ex_commet: boolean;
   begin
      -- Miramos si hay comentarios de cualquiera de las calificaciones
      -- y si las hay, imprimimos el comentario mas antiguo
      Put("--Comentarios para "); Put(str); Put_Line("--");
      ex_commet:=false;
      if existeix_comentari(c, cat, k, Bo) then Put("--Bueno: "); Put_Line(consultar_comentari (c, cat, k, Bo)); ex_commet:=true; end if;
      if existeix_comentari(c, cat, k, Mitja) then Put("--Medio: "); Put_Line(consultar_comentari (c, cat, k, Mitja)); ex_commet:=true; end if;
      if existeix_comentari(c, cat, k, Dolent) then Put("--Malo: "); Put_Line(consultar_comentari (c, cat, k, Dolent)); ex_commet:=true; end if;
      if not ex_commet then Put_Line("No hay comentarios"); end if;
   end print_comments;
begin

   -- Codigos de los platos y bebidas de la temporada otono/invierno
   key1(1..8) := "DATILES@";
   key2(1..7) := "PINCHO@";
   key3(1..7) := "OLIVAS@";
   key4(1..14) := "CREMAVERDURAS@";
   key5(1..10) := "GARBANZOS@";
   key6(1..9) := "LENTEJAS@";
   key7(1..8) := "MERLUZA@";
   key8(1..6) := "POLLO@";
   key9(1..7) := "SALMON@";
   key10(1..10) := "CREMACAFE@";
   key11(1..6) := "TARTA@";
   key12(1..9) := "ESTRELLA@";
   key13(1..5) := "VINO@";
   key14(1..5) := "AGUA@";
   key15(1..9) := "COCACOLA@";

   -- Nombre de los platos y bebidas
   str1:= to_unbounded_string("Datiles con bacon");
   str2:= to_unbounded_string("Pincho morruno");
   str3:= to_unbounded_string("Olivas");
   str4:= to_unbounded_string("Crema de verduras");
   str5:= to_unbounded_string("Garbanzos");
   str6:= to_unbounded_string("Lentejas");
   str7:= to_unbounded_string("Merluza");
   str8:= to_unbounded_string("Pollo");
   str9:= to_unbounded_string("Salmon");
   str10:= to_unbounded_string("Crema de cafe");
   str11:= to_unbounded_string("Tarta de higos");
   str12:= to_unbounded_string("Estrella de chocolate");
   str13:= to_unbounded_string("Vino");
   str14:= to_unbounded_string("Agua");
   str15:= to_unbounded_string("Coca Cola");

   -- Inicializamos el TAD carta
   carta_buida(carta_1);

   -- Introducimos los platos y bebidas en la estructura
   posar_element (carta_1, Entrant, key1, str1);
   posar_element (carta_1, Entrant, key2, str2);
   posar_element (carta_1, Entrant, key3, str3);
   posar_element (carta_1, Primer, key4, str4);
   posar_element (carta_1, Primer, key5, str5);
   posar_element (carta_1, Primer, key6, str6);
   posar_element (carta_1, Segon, key7, str7);
   posar_element (carta_1, Segon, key8, str8);
   posar_element (carta_1, Segon, key9, str9);
   posar_element (carta_1, Postres, key10, str10);
   posar_element (carta_1, Postres, key11, str11);
   posar_element (carta_1, Postres, key12, str12);
   posar_element (carta_1, Begudes, key13, str13);
   posar_element (carta_1, Begudes, key14, str14);
   posar_element (carta_1, Begudes, key15, str15);

   -- Comentarios
   strc1:= to_unbounded_string("Es lo peor que he comido");
   strc2:= to_unbounded_string("Es acceptable");
   strc3:= to_unbounded_string("Ha estado muy bueno");
   strc4:= to_unbounded_string("Estaba cruda la comida");
   strc5:= to_unbounded_string("Voy a repetir");

   -- Introducimos los platos y bebidas en la estructura
   posar_comentari (carta_1, Entrant, key2, Dolent, strc1);
   posar_comentari (carta_1, Entrant, key2, Dolent, strc4);
   posar_comentari (carta_1, Entrant, key2, Bo, strc3);
   posar_comentari (carta_1, Primer, key5, Mitja, strc2);
   posar_comentari (carta_1, Segon, key8, Bo, strc3);
   posar_comentari (carta_1, Postres, key12, Dolent, strc4);
   posar_comentari (carta_1, Begudes, key15, Bo, strc5);

   -- Imprimimos la carta por categorias de otono/invierno
   Put_Line("----Carta otono/invierno----");
   Put_Line("--Entrantes--");
   imprimir_elements(carta_1, Entrant);
   -- Imprimimos los comentarios si los hubiese
   print_comments(carta_1, Entrant, key2, str2);

   Put_Line("--Primeros--");
   imprimir_elements(carta_1, Primer);
   -- Imprimimos los comentarios si los hubiese
   print_comments(carta_1, Primer, key5, str5);

   Put_Line("--Segundos--");
   imprimir_elements(carta_1, Segon);
   -- Imprimimos los comentarios si los hubiese
   print_comments(carta_1, Segon, key8, str8);

   Put_Line("--Postres--");
   imprimir_elements(carta_1, Postres);
   -- Imprimimos los comentarios si los hubiese
   print_comments(carta_1, Postres, key12, str12);

   Put_Line("--Bebidas--");
   imprimir_elements(carta_1, Begudes);
   -- Imprimimos los comentarios si los hubiese
   print_comments(carta_1, Begudes, key15, str15);

   -- Eliminamos comentarios
   eliminar_comentari(carta_1, Entrant, key2, Dolent);

   -- Eliminamos platos de la carta
   eliminar_elements(carta_1, Entrant, key2);
   eliminar_elements(carta_1, Primer, key4);
   eliminar_elements(carta_1, Primer, key5);
   eliminar_elements(carta_1, Primer, key6);
   eliminar_elements(carta_1, Postres, key11);

   -- Creamos los nuevos platos de la carta
   key2(1..8) := "ALLIOLI@";
   key4(1..9) := "ENSADALA@";
   key5(1..9) := "GAZPACHO@";
   key6(1..10) := "CREMAFRIA@";
   key11(1..7) := "HELADO@";
   str2:= to_unbounded_string("Pan con allioli");
   str4:= to_unbounded_string("Ensalada mixta");
   str5:= to_unbounded_string("Gazpacho");
   str6:= to_unbounded_string("Crema fria");
   str11:= to_unbounded_string("Helado de chocolate");

   -- Ponemos los nuevos platos en la carta
   posar_element (carta_1, Entrant, key2, str2);
   posar_element (carta_1, Primer, key4, str4);
   posar_element (carta_1, Primer, key5, str5);
   posar_element (carta_1, Primer, key6, str6);
   posar_element (carta_1, Postres, key11, str11);

   -- Imprimimos la carta por categorias de primavera/verano
   Put_Line("----Carta primavera/verano----");
   Put_Line("--Entrantes--");
   imprimir_elements(carta_1, Entrant);
   -- Imprimimos los comentarios si los hubiese
   print_comments(carta_1, Entrant, key2, str2);

   Put_Line("--Primeros--");
   imprimir_elements(carta_1, Primer);
   -- Imprimimos los comentarios si los hubiese
   print_comments(carta_1, Primer, key5, str5);

   Put_Line("--Segundos--");
   imprimir_elements(carta_1, Segon);
   -- Imprimimos los comentarios si los hubiese
   print_comments(carta_1, Segon, key8, str8);

   Put_Line("--Postres--");
   imprimir_elements(carta_1, Postres);
   -- Imprimimos los comentarios si los hubiese
   print_comments(carta_1, Postres, key12, str12);

   Put_Line("--Bebidas--");
   imprimir_elements(carta_1, Begudes);
   -- Imprimimos los comentarios si los hubiese
   print_comments(carta_1, Begudes, key15, str15);

exception
   when dcarta.bad_use => Put_Line("Mala utilizacion del TAD carta.");
   when dcarta.space_overflow => Put_Line("El TAD carta tiene demasiados nodos.");
   when dcarta.already_exists => Put_Line("El elemento ya se encuentra en la carta.");
   when dcarta.does_not_exist => Put_Line("El elemento no se encuentra en la carta.");
end Restaurant;
