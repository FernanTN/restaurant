with dtrie;
with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;

package dcarta is
   type carta is limited private;
   type tcategoria is (Entrant, Primer, Segon, Postres, Begudes);
   type tqualificacio is (Bo, Mitja, Dolent);
   subtype component is Character range '@' .. 'Z';
   type index is new Natural range 1..50;
--     type index is new natural;
   type tcodi is array(index) of component;

   bad_use: exception;
   already_exists: exception;
   does_not_exist: exception;
   space_overflow: exception;
   procedure carta_buida (c: out carta);
   procedure posar_element (c: in out carta; cat: in tcategoria; k: in tcodi; nom: in Unbounded_String);
   procedure imprimir_elements(c: in carta; cat: in tcategoria);
   procedure eliminar_elements(c: in out carta; cat: in tcategoria; k: in tcodi);
   procedure posar_comentari(c: in out carta; cat: in tcategoria; k: in tcodi; q : in tqualificacio; comentari: in Unbounded_String);
   function consultar_comentari (c: in carta; cat : in tcategoria; k: in tcodi; q: in tqualificacio ) return Unbounded_String;
   function existeix_comentari (c: in carta; cat: in tcategoria; k: in tcodi; q: in tqualificacio) return Boolean;
   procedure eliminar_comentari (c: in out carta; cat: in tcategoria; k: in tcodi; q : in tqualificacio);
private
   -- Instanciacion del paquete trie, el cual hemos modificado
   -- para el presente  problema
   package dtrie_item is new dtrie(key_component => component, key_index => index, key => tcodi, tqualificacio => tqualificacio);
   use dtrie_item;

   -- El TAD carta es una estructura array indexado por claves, categoria,
   -- donde cada uno de los elementos del array son punteros a tries
   type existencia is array(tcategoria) of boolean;
   type contenido is array(tcategoria) of trie;
   type carta is record
      e: existencia;
      co: contenido;
   end record;
end dcarta;

