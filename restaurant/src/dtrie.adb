with Ada.Text_IO; use Ada.Text_IO;
with Ada.Strings.Unbounded.Text_IO;
use Ada.Strings.Unbounded.Text_IO;

package body dtrie is
   --definiciones en el body del paquete
   mk: constant key_component:= key_component'first;
   lastc: constant key_component:= key_component'last;
   i0: constant key_index := key_index'first;

   procedure cvacio(s: out trie) is
      raiz: pnode renames s.raiz;
   begin
      -- Creamos un nuevo nodo tipo interior
      raiz:= new node(interior);
      -- se inicializa todo a null
      raiz.ti := (others => null); --inicializa array
   end cvacio;

   -- Funcion para verificar que el trie esta vacio
   function is_empty(s: in trie) return boolean is
      raiz: pnode renames s.raiz;
      c: key_component;
      nd: integer; -- numero de descendientes
   begin
      c:= mk; nd:= 0;
      while c/=lastc loop
         if raiz.ti(c)/= null then nd:= nd+1; end if;
         c:= key_component'succ(c);
      end loop;
      -- comprobar el ultimo caso c=lastc
      if raiz.ti(c)/= null then nd:= nd+1; end if;
      -- Si nd es cero, el trie esta vacio
      return nd=0;
    end is_empty;

   function existe(s: in trie; k: in key) return boolean is
      raiz: pnode renames s.raiz;
      p: pnode;
      i: key_index;
      c: key_component; -- a copy of k(i)
   begin
      -- primer indice de la clave
      -- primer caracter de la clave en c
      p:= raiz; i:= i0; c:= k(i);
      -- mientras la clave no acabe y haya continuidad en la busqueda
      -- es decir que en el array en la posicion c no apunte a null
      while c/=mk and p.ti(c)/= null loop
         p:= p.ti(c); i:= i+1; c:= k(i);
      end loop;
      --comprueba que la clave se haya evaluado toda c=mk y que mk apunta
      --al nodo leaf, que lo hacemos mirando el tipo de nodo
      return c=mk and then p.ti(mk).tn=leaf;
   end existe;

   procedure poner(s: in out trie; k: in key; x: in Unbounded_String) is
      raiz: pnode renames s.raiz;
      p, r: pnode;
      i: key_index;
      c: key_component; -- copia de k(i)
   begin
      -- Comprobamos si existe la clave en el trie y si existe
      -- elevamos la excepcion que existe
      if existe(s, k) then raise already_exists; end if;
      p:= raiz; i:= i0; c:= k(i);
      while c/=mk loop
         if p.ti(c)=null then -- No existe hijo para c: crearlo
            r:= new node(interior);
            r.ti:= (others => null);
            p.ti(c):= r; --se apunta al nuevo node
         end if ;
         p:= p.ti(c); i:= i+1; c:= k(i);
      end loop;
      r:= new node(leaf); -- creamos el node leaf donde esta la informacion del nodo
      r.name:=x; -- Insertamos el nombre del plato
      -- Incializamos cada una de las colas para los comentarios
      for q in tqualificacio loop
         empty(r.comments(q));
      end loop;
      p.ti(mk):= r; -- p.ti(mk) apunta al node leaf
   exception
      when storage_error => raise space_overflow;
   end poner;

   function unico_desc(p: in pnode) return boolean is
      c: key_component;
      nd: integer; -- numero de descendientes
   begin
      c:= mk; nd:= 0;
      while c/=lastc and nd<2 loop
         if p.ti(c)/= null then nd:= nd+1; end if ;
         c:= key_component'succ(c);
      end loop;
      -- comprobar el ultimo caso c=lastc
      if p.ti(c)/= null then nd:= nd+1; end if ;
      return nd<2;
   end unico_desc;

   procedure borrar(s: in out trie; k: in key) is
      raiz: pnode renames s.raiz;
      p, r: pnode; -- para guardar la bifurcacion r: puntero
      i: key_index;
      c, cr: key_component; --para guardar la bifurcacion cr. caracter
   begin
      -- Comprobamos si existe la clave en el trie y si no,
      -- elevamos la excepcion no existe
      if not existe(s, k) then raise does_not_exist; end if;
      p:= raiz; i:= i0; c:= k(i); r:= p; cr:= c;
      while c/=mk and p.ti(c)/= null loop
         if not unico_desc(p) then r:= p; cr:= c; end if ;
         p:= p.ti(c); i:= i+1; c:= k(i);
      end loop;
      if c=mk and then p.ti(mk).tn=leaf then -- elemento existe
         if unico_desc(p) then -- no es un prefijo de una clave mas larga
            r.ti(cr):= null; -- desenlazar la rama
         else p.ti(mk):= null; -- quitar solo la rama $, porque es prefijo
         end if ;
      end if ;
   end borrar;

   -- Procedimientos para los comentarios --
   -- Introducimos un nuevo comentario en la cola, segun la calificacion
   procedure put_comment(s: in trie; k: in key; q: in tqualificacio; comment: in Unbounded_String) is
      raiz: pnode renames s.raiz;
      p: pnode;
      i: key_index;
      c: key_component; -- a copy of k(i)
   begin
      -- Comprobamos si existe la clave en el trie y si no,
      -- elevamos la excepcion no existe
      if not existe(s, k) then raise does_not_exist; end if;
      -- primer indice de la clave
      -- primer caracter de la clave en c
      p:= raiz; i:= i0; c:= k(i);
      -- mientras la clave no acabe y haya continuidad en la busqueda
      -- es decir que en el array en la posicion c no apunte a null
      while c/=mk and p.ti(c)/= null loop
         p:= p.ti(c); i:= i+1; c:= k(i);
      end loop;
      -- Ponemos el comentario dentro de la cola
      put(p.ti(mk).comments(q), comment);
      -- Capturamos la excepcion de la cola
      exception
         when d_queue.space_overflow => raise dtrie.space_overflow;
   end put_comment;

   -- Obtenemos el comentario mas antiguo que esta dentro de la cola de una
   -- calificacion dada
   procedure query_comment(s: in trie; k: in key; q : in tqualificacio; comment: out Unbounded_String) is
      raiz: pnode renames s.raiz;
      p: pnode;
      i: key_index;
      c: key_component; -- a copy of k(i)
   begin
      -- Comprobamos si existe la clave en el trie y si no,
      -- elevamos la excepcion no existe
      if not existe(s, k) then raise does_not_exist; end if;
      -- primer indice de la clave
      -- primer caracter de la clave en c
      p:= raiz; i:= i0; c:= k(i);
      -- mientras la clave no acabe y haya continuidad en la busqueda
      -- es decir que en el array en la posicion c no apunte a null
      while c/=mk and p.ti(c)/= null loop
         p:= p.ti(c); i:= i+1; c:= k(i);
      end loop;
      -- Obtenenos el comentario mas antiguo que
      -- existe en la cola
      comment:= get_first(p.ti(mk).comments(q));
      -- Capturamos la excepcion de la cola
      exception
         when d_queue.bad_use => raise dtrie.bad_use;
   end query_comment;

   -- Comprobamos si la cola contiene comentarios o no, dada una calificacion
   function exist_comment(s: in trie; k: in key; q : in tqualificacio) return boolean is
      raiz: pnode renames s.raiz;
      p: pnode;
      i: key_index;
      c: key_component; -- a copy of k(i)
   begin
      -- Comprobamos si existe la clave en el trie y si no,
      -- elevamos la excepcion no existe
      if not existe(s, k) then raise does_not_exist; end if;
      -- primer indice de la clave
      -- primer caracter de la clave en c
      p:= raiz; i:= i0; c:= k(i);
      -- mientras la clave no acabe y haya continuidad en la busqueda
      -- es decir que en el array en la posicion c no apunte a null
      while c/=mk and p.ti(c)/= null loop
         p:= p.ti(c); i:= i+1; c:= k(i);
      end loop;
      -- Devolvemos true si existen comentarios y false
      -- si la cola esta vacia
      return not is_empty(p.ti(mk).comments(q));
   end exist_comment;

   -- Eliminamos el comentario mas antiguo, dada una calificacion
   procedure delete_comment(s: in trie; k: in key; q : in tqualificacio) is
      raiz: pnode renames s.raiz;
      p: pnode;
      i: key_index;
      c: key_component; -- a copy of k(i)
   begin
      -- Comprobamos si existe la clave en el trie y si no,
      -- elevamos la excepcion no existe
      if not existe(s, k) then raise does_not_exist; end if;
      -- primer indice de la clave
      -- primer caracter de la clave en c
      p:= raiz; i:= i0; c:= k(i);
      -- mientras la clave no acabe y haya continuidad en la busqueda
      -- es decir que en el array en la posicion c no apunte a null
      while c/=mk and p.ti(c)/= null loop
         p:= p.ti(c); i:= i+1; c:= k(i);
      end loop;
      --Eliminamos de la cola el comentario mas antiguo
      rem_first(p.ti(mk).comments(q));
      -- Capturamos la excepcion de la cola
   exception
         when d_queue.bad_use => raise dtrie.bad_use;
   end delete_comment;

   -- ITERADOR --
   --busca el primer nodo no nulo
   procedure firstbranch(p: in pnode; c: out key_component; found: out boolean) is
   begin
     c:= mk; found:= (p.ti(c)/=null);
      while c < lastc and not found loop
         c:= key_component'succ(c);
         found:= (p.ti(c)/=null);
      end loop;
   end firstbranch;

   procedure first(s: in trie; it: out iterator) is
      raiz: pnode renames s.raiz;
      pth: path renames it.pth;
      k: key renames it.k;
      i: key_index renames it.i;
      c: key_component;
      p: pnode;
      found: boolean;
   begin
      p:= raiz; i:= i0;
      firstbranch(p, c, found); --busca primer nodo no nulo
      while found and c/=mk loop
         pth(i):= p; k(i):= c; i:= i+1;
         p:= p.ti(c);
         firstbranch(p, c, found);
      end loop;
      pth(i):= p; k(i):= mk; --autopuntero y final a la clave
   end first;

   procedure nextbranch(p: in pnode; c: in out key_component; found: out boolean) is
   begin
      found:= false;
      while c<lastc and not found loop
         c:= key_component'succ(c);
         found:= (p.ti(c)/=null);
      end loop;
   end nextbranch;

   procedure next(s: in trie; it: in out iterator) is
      raiz: pnode renames s.raiz;
      pth: path renames it.pth;
      k: key renames it.k;
      i: key_index renames it.i;
      c: key_component;
      p: pnode;
      found: boolean;
   begin
      if k(i0)=mk then raise bad_use; end if ;
      p:= pth(i); c:= k(i); --posicion y caracter
      nextbranch(p, c, found);
      while not found and i>1 loop --busco siguiente bifurcacion hacia arriba
         i:= i-1; p:= pth(i); c:= k(i);
         nextbranch(p, c, found);
      end loop;
      while found and c/=mk loop --vuelvo a bajar para abajo.
         pth(i):= p; k(i):= c; i:= i+1;
         p:= p.ti(c);
         firstbranch(p, c, found);
      end loop;
      pth(i):= p; k(i):= mk;
   end next;

   function is_valid(it: in iterator) return boolean is
      k: key renames it.k;
   begin
      return k(i0)/=mk;
   end is_valid;

   procedure get(s: in trie; it: in iterator; k: out key; x: out Unbounded_String) is
      i: key_index renames it.i;
   begin
      if it.k(i0)=mk then raise bad_use; end if;
      k:= it.k;
      x:= it.pth(i).ti(mk).name;
   end get;

end dtrie;
