generic
   type item is private;
package dqueue is
   type queue is limited private;
   bad_use: exception;
   space_overflow: exception;
   procedure empty (qu: out queue);
   procedure put (qu: in out queue; x: in item);
   procedure rem_first(qu: in out queue);
   function get_first(qu: in queue) return item;
   function is_empty(qu: in queue) return boolean;

private
   type node;
   type pnode is access node;
   type node is record
      x: item;
      sig: pnode;
   end record;

   type queue is record
      p, q: pnode;
   end record;
end dqueue;
