with dqueue;
with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;

generic
   type key_component is (<>); -- tipo discreto componentes
   type key_index is range <>; -- rango de enteros, indice sobre la clave
   type key is array(key_index) of key_component;
   type tqualificacio is (<>);
package dtrie is
   type trie is limited private;
   type iterator is private;

   bad_use: exception;
   already_exists: exception;
   does_not_exist: exception;
   space_overflow: exception;
   procedure cvacio (s: out trie);
   function is_empty(s: in trie) return boolean;
   procedure poner (s: in out trie; k: in key; x: in Unbounded_String);
   function existe(s: in trie; k: in key) return boolean;
   procedure borrar(s: in out trie; k: in key);

   -- Procedimientos y funciones para los comentarios
   procedure put_comment(s: in trie; k: in key; q : in tqualificacio; comment: in Unbounded_String);
   procedure query_comment(s: in trie; k: in key; q : in tqualificacio; comment: out Unbounded_String);
   function exist_comment(s: in trie; k: in key; q : in tqualificacio) return boolean;
   procedure delete_comment(s: in trie; k: in key; q : in tqualificacio);

   -- Iterador
   procedure first (s: in trie; it: out iterator);
   procedure next (s: in trie; it: in out iterator);
   function is_valid (it: in iterator) return boolean;
   procedure get (s: in trie; it: in iterator; k: out key; x: out Unbounded_String);
private

   -- Instanciamos el paquete cola generico
   package d_queue is new dqueue(item => Unbounded_String);
   use d_queue;

   type node;
   type pnode is access node;
   -- Nuestro trie tiene dos tipos de nodos, uno interior donde tenemos los
   -- arrys de nodos y otro tipo nodo leaf donde guardamos el nombre del plato y
   -- tambien tenemos un array indexado por claves para los tres tipos de comentarios
   type tiponode is (leaf, interior);
   type t_node is array(key_component) of pnode;
   -- Array indexado por claves, para los comentarios
   type array_comment is array(tqualificacio) of queue;

   type node(tn: tiponode) is record
      case tn is
         when leaf =>
            name: Unbounded_String;
            comments: array_comment;
         when interior =>
            ti: t_node;
      end case;
   end record;

   type path is array (key_index) of pnode;
   type iterator is record
      pth : path;
      k : key;
      i : key_index;
   end record;

   type trie is
      record
         raiz: pnode;
      end record;
end dtrie;
