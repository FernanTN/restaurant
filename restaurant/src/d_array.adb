with dqueue;
with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;

package body d_array is
   procedure cvacio(s: out array_coment) is --O(n)
      e: existencia renames s.e; begin
      for k in key loop e(k):= false; end loop; end cvacio;

   procedure poner(s: in out array_coment; k: in key; x: in Unbounded_String) is
      e: existencia renames s.e;
      c: contenido renames s.c;
   begin
      if e(k) then raise ya_existe; end if;
      e(k):= true;
--        c(k):= x;
   end poner;

   procedure actualiza(s: in out array_coment; k: in key; x: in Unbounded_String) is
      e: existencia renames s.e;
      c: contenido renames s.c;
   begin
      if not e(k) then raise no_existe; end if;
--        c(k):= x;
   end actualiza;

   procedure consultar( s: in array_coment; k: in key; x: out Unbounded_String) is
      e: existencia renames s.e;
      c: contenido renames s.c;
   begin
      if not e(k) then raise no_existe; end if;
      --        x:= c(k);
      x:= to_unbounded_string("Merluza");
   end consultar;

   procedure borrar(s: in out array_coment; k: in key) is
      e: existencia renames s.e;
   begin
      if not e(k) then raise no_existe; end if;
      e(k):= false;
   end borrar;
end d_array;
