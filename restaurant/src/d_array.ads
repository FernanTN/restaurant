with dqueue;
with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;

generic
   type key_array is (<>);
--     type item is private;
package d_array is
   type array_coment is limited private;
   ya_existe: exception;
   no_existe: exception;
   espacio_desbordado: exception;
   procedure cvacio (s: out array_coment);
   procedure poner (s: in out array_coment; k: in key_array; x: in Unbounded_String);
   procedure consultar(s: in array_coment; k: in key_array; x: out Unbounded_String);
   procedure borrar(s: in out array_coment; k: in key_array);
   procedure actualiza(s: in out array_coment; k: in key_array; x: in Unbounded_String);
private

   package d_queue is new dqueue(item => Unbounded_String);
   use d_queue;

   type existencia is array(key_array) of boolean;
   type contenido is array(key_array) of item;
   type array_coment is record
      e: existencia ;
      c: contenido ;

   end record;
end d_array;
