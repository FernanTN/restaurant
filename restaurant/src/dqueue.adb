package body dqueue is
   procedure empty(qu: out queue) is
      p: pnode renames qu.p;
      q: pnode renames qu.q;
   begin
      p:= null; q:= null;
   end empty;

   function is_empty(qu: in queue) return boolean is
      q: pnode renames qu.q;
   begin
      return q=null;
   end is_empty;

   function get_first(qu: in queue) return item is
      q: pnode renames qu.q;
   begin
      return q.x;
   exception
      when constraint_error => raise bad_use;
   end get_first;

   procedure put(qu: in out queue; x: in item) is
      p: pnode renames qu.p;
      q: pnode renames qu.q;
      r: pnode;
   begin
      r:= new node;
      r.all:= (x, null);
      if p=null then -- qu esta vacia
         p:= r; q:= r;
      else
         p.sig:= r; p:= r;
      end if;
   exception
      when storage_error => raise space_overflow;
   end put;

   procedure rem_first(qu: in out queue) is
      p: pnode renames qu.p;
      q: pnode renames qu.q;
   begin
      q:= q.sig;
      if q=null then p:= null;
      end if;
   exception
      when constraint_error => raise bad_use;
   end rem_first;

end dqueue;
